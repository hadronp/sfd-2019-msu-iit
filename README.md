# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This contains the presentation slides and demo Jupyter Notebook and Mobile Legends: Bang Bang dataset used in the 2019 Software Freedom Day talk 21 September 2019 at MSU-IIT, Iligan City, PH

## Slides ###

* navigate to the ```sfd2019-datascience-slides``` directory
* Open index.html on your preferred browser ()

### Demo ###

* Install conda for your platform https://conda.io/miniconda.html
* navigate to the ```ds-mlbb``` directory
* in the CLI type : ```conda create --name ds-mlbb pip python=3.7```
* to activate the virtual environment type: ```source activate ds-mlbb```
* Install packages and dependencies: ```pip install -r requirements.txt```
* run the JupyterLab notebook:  ```jupyter lab```
* open the notebook in the browser explorer 

### Acknowledgement ###

The text portions of the notebook are adapted from Sarah Guido's material delivered from 
Hands-on Data Analysis with Python - PyCon 2015. Merci. :)

### Who do I talk to? ###

* Repo owner or admin https://www.facebook.com/aryan.limjap
* https://www.facebook.com/groups/itgpytsada/
